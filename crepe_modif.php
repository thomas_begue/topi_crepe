<?php
session_start();
if (!empty($_SESSION['pseudo'])) {
    if ($_SESSION['id_role'] == 1) {
?>
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">

        <?php
        $nomcrepe = "";
        $ingredient = "";
        $prix = "";
        $sucresale = "";
        $id = "";
        require 'connexionbdd.php';
        if (isset($_GET["id"])) {
            $id = $_GET["id"];
            if (!empty($id) and is_numeric($id)) {
                $modif = $bdd->query("SELECT *  FROM crepes WHERE id=$id");
                $recup_crepe = $modif->fetchAll();
                $nomcrepe = $recup_crepe[0]['nom_crepe'];
                $ingredient = $recup_crepe[0]['ingredient'];
                $prix = $recup_crepe[0]['prix'];
                $sucresale = $recup_crepe[0]['id_sucresale'];
                $id = $recup_crepe[0]['id'];
            }
        }
        if (isset($_POST['formmodifcrep'])) {
            if (isset($_POST['nomcrep']) and isset($_POST['ingredient']) and isset($_POST['prix'])) {
                //L'ajout du slashes permet d'ajouter un aspostrophe dans la BDD lorsqu'on modifie le nom d'une crepe
                $nomcrepe = addslashes($_POST['nomcrep']);
                $ingredient = $_POST['ingredient'];
                $prix = $_POST['prix'];
                $sucresale = $_POST['sucresale'];

                if (!empty($nomcrepe) and !empty($ingredient) and !empty($prix) and !empty($sucresale) and !empty($id) && is_numeric($id)) {
                    //Modifie la ligne en question dans la BDD
                    $modif = $bdd->prepare("UPDATE crepes SET nom_crepe='$nomcrepe', ingredient='$ingredient', prix='$prix', id_sucresale='$sucresale' WHERE id=$id");
                    $modif->execute();
                    header("Location:modifier_suppr.php");
                }
            }
        }

        ?>





        <!DOCTYPE html>
        <html lang="fr">

        <head>
            <link rel="stylesheet" href="assets/css/style.css">
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
        </head>

        <body>
            <main id="modif-crepe">
                <?php require 'menu.php'; ?>
                <div class="grid-container">
                    <div class="grid-x grid-margin-x grid-padding-x">
                        <div class="cell small-6 medium-8 large-12">
                            <h2 class="aligntext">Modifier une crêpe</h2>
                            <form method="POST" action="">
                                <!--Un tableau est utilisé pour la fluidité et la mise en page du formulaire d'inscription -->
                                <table>
                                    <tr>
                                        <td>
                                            <!-- Le label permet dafficher lintitulé juste à coté du champ à remplir -->
                                            <label>Nom : </label>
                                        </td>
                                        <td>
                                            <input type="text" placeholder="Nom crêpe" name="nomcrep" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Ingrédient : </label>
                                        </td>
                                        <td>
                                            <TEXTAREA type="text" name="ingredient" placeholder="Veuillez mettre les ingrédients" class="ingredtext"></TEXTAREA>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Prix : </label>
                                        </td>
                                        <td>
                                            <input type="text" placeholder="Prix €" name="prix" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Veuillez choisir entre sucré ou salé</label>
                                        </td>
                                        <td>
                                            <select name="sucresale">
                                                <option value="1">Sucré</option>
                                                <option value="2">Salé</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <input class="success button" name="formmodifcrep" type="submit" value="Je modifie cette crêpe">
                                        </td>
                                    </tr>
                                </table>
                            </form>
                    <?php

                }
            } else {
                echo "<script>alert(\"Vous n'êtes pas administrateur\")</script>";
                header("Location:index.php");
            }
                    ?>

                        </div>
                    </div>
                </div>
            </main>
        </body>

        </html>