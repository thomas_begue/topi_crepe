<?php
//Permet de se connecter à la base de donnée
require 'connexionbdd.php';
//Permet d'activer le code au moment ou l'on cliquer sur le bouton 'Je m'inscris'
if (isset($_POST['forminscription'])) {
    //Ici va vérifié si chaque input(Champ) est remplie
    if (!empty($_POST['pseudo']) and !empty($_POST['mail']) and !empty($_POST['mail2']) and !empty($_POST['mdp']) and !empty($_POST['mdp2'])) {
        //Permet d'éviter l'injonction de code ou de requête pour plus de sécurité
        $pseudo = htmlspecialchars($_POST['pseudo']);
        $mail = htmlspecialchars($_POST['mail']);
        $mail2 = htmlspecialchars($_POST['mail2']);
        $mdp = sha1($_POST['mdp']);
        $mdp2 = sha1($_POST['mdp2']);

        //Permet de regarder si le pseudo choisi ne dépasse pas 250 caractères
        $pseudolength = strlen($pseudo);
        if ($pseudolength <= 250) {
            //Permet de vérifié si le pseudo existe déjà ou non
            $existpseudo = $bdd->prepare("SELECT * FROM user WHERE pseudo = ?");
            $existpseudo->execute(array($pseudo));
            $pseudoexist = $existpseudo->RowCount();
            if ($pseudoexist == 0) {
                //Si le pseudo est en dessous des 250 caractères alors vérifie si le mail 1 correspond à la vérification du mail
                if ($mail == $mail2) {
                    //Si tout est bon on vérifie si il s'agit bien d'une adresse mail valide
                    if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                        //Permet de vérifié si l'adresse mail est pas déjà existante
                        $existmail = $bdd->prepare("SELECT * FROM user WHERE mail = ?");
                        $existmail->execute(array($mail));
                        $mailexist = $existmail->RowCount();
                        if ($mailexist == 0) {
                            //Permet de vérifié si les mots de passes choisis corresponde entre eux
                            if ($mdp == $mdp2) {
                                //Permet d'insérer dans la base de donnée l'utilisateur qui vient de remplir le formulaire-------Le 2 permet d'ajouter comme role à l'utilisateur "Inscrit"
                                $insert_user = $bdd->prepare("INSERT INTO user(pseudo, mail, mdp, id_role) VALUES(?, ?, ?, 2)");
                                $insert_user->execute(array($pseudo, $mail, $mdp));
                                $erreur = "Votre compte a bien été créé !";
                            } else {
                                $erreur = "Vos mots de passe ne sont pas identiques !";
                            }
                        } else {
                            $erreur = "L'adresse mail est déjà utilisée !";
                        }
                    } else {
                        $erreur = "Votre adresse mail n'est pas valide !";
                    }
                } else {
                    $erreur = "Vos adresses mails ne sont pas identiques !";
                }
            } else {
                $erreur = "Le pseudo est déjà utilisé !";
            }
        } else {
            $erreur = "Votre pseudo ne doit pas dépasser 250 caractères !";
        }
    } else {
        //Permet d'afficher un message d'erreur si les champs du formulaire d'inscription ne sont pas remplie
        $erreur = "Veuillez remplir tous les champs";
    }
}
