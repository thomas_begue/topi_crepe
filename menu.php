<div class="grid-container">
    <div class="grid-x grid-margin-x grid-padding-x align-center">
        <div class="cell small-6 medium-8 large-12">
            <ul class="menu align-center">
                <li class="menu-text">
                    <h4 class="change_title_menu">Topi'Crepe</h4>
                </li>
                <li><a class="menu_change" href="index.php">Accueil</a></li>
                <li><a class="menu_change" href="nos_crepes.php">Nos crêpes</a></li>
                <li><a class="menu_change" href="nos_avis.php">Avis</a></li>
                <!-- Si la personne est connecter alors affiche que deconnexion+ajout crepe + modif/suppr, sinon affiche inscription et connexion -->
                <?php if (!empty($_SESSION['pseudo'])) { ?>
                    <li><a class="menu_change" href="deconnexion.php">Déconnexion</a></li>
                    <?php if ($_SESSION['id_role'] == 1) { ?>
                        <li><a class="menu_change" href="crepe_ajout.php">Ajout-Crêpe</a></li>
                        <li><a class="menu_change" href="modifier_suppr.php">Modif/Suppr-Crêpe</a></li>
                        <li><a class="menu_change" href="ajout_news.php">Ajouter une news</a></li>
                    <?php }
                } else { ?>
                    <li><a class="menu_change" href="inscription.php">Inscription</a></li>
                    <li><a class="menu_change" href="connexion.php">Connexion</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>