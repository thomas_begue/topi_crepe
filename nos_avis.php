<!DOCTYPE html>
<html lang="fr">

<head>
    <?php require 'connexionbdd.php' ?>
    <?php session_start(); ?>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
    <?php require 'insert_avis.php'; ?>
</head>

<body>
    <main id="avis-site">
        <?php require 'menu.php'; ?>
        <?php if (!empty($_SESSION['pseudo'])) { ?>
            <div class="grid-container">
                <div class="grid-x grid-margin-x grid-padding-x">
                    <div class="cell small-6 medium-8 large-12">
                        <h2 class="aligntext">Veuillez donner votre avis :</h2>
                    </div>
                    <div class="cell medium-4 large-4">
                    </div>
                    <div class="cell medium-4 large-4">
                        <!-- J'affiche un formulaire pour ajouter un avis -->
                        <form method="POST" action="">
                            <input type="text" name="nompersonne" placeholder="Nom Prénom" />
                            <TEXTAREA type="text" name="commentaire" placeholder="Veuillez laisser un commentaire" class="ingredtext"></TEXTAREA>

                            <input class="success button" type="submit" name="formavis" value="Envoyer l'avis !" />
                    </div>
                    <div class="cell medium-4 large-4">
                    </div>
                    <div class="cell small-6 medium-8 large-12">
                        <div class="erreur">
                            <?php
                            if (isset($erreur)) {
                                echo $erreur;
                            } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="grid-x grid-margin-x grid-padding-x">
                <div class="cell small-6 medium-8 large-12">
                    <h2 class="aligntext">Voici les avis :</h2>
                </div>
            </div>
            </div>
            <div class="grid-container">
                <div class="grid-x grid-margin-x grid-padding-x">

                    <?php
                    //Ici je fais la requete pour allez chercher les avis dans la BDD pour ensuite les affichers
                    // ORDER BY ID DESC LIMIT 0,3

                    $reponse = $bdd->query('SELECT nom_personne, commentaire FROM avis');
                    while ($recup_avis = $reponse->fetch()) {
                    ?>
                        <div class="cell medium-4 large-4">
                            <div class="card" style="width: 250px;">
                                <div class="card-section">
                                    <h5><?= $recup_avis['nom_personne']; ?> nous donne son avis :</h5>
                                    <p><?= $recup_avis['commentaire']; ?></p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>

            </div>
    </main>
</body>

</html>