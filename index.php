<?php
require 'lib/autoload.php';
$db = DBFactory::getMysqlConnexionWithPDO();
$manager = new NewsManagerPDO($db);
require_once 'templates-parts/header.php';
?>


<div class="grid-container">
    <div class="grid-x grid-margin-x grid-padding-x">
        <?php
        foreach ($manager->getList(0, 1) as $news) {
            if (strlen($news->contenu()) <= 200) {
                $contenu = $news->contenu();
                $i = 0;
            } else {
                $debut = substr($news->contenu(), 0, 200);
                $debut = substr($debut, 0, strrpos($debut, ' ')) . '...';

                $contenu = $debut;
                $i = 0;
            }

        ?>

            <div class="cell medium-4 large-4">
                <div class="card contour">
                    <div class="card-section">
                        <img src="<?= $news->imagenews(); ?>" class="img_card">
                    </div>
                    <div class="card-section">
                        <p class="align-text"><?= nl2br($contenu); ?></p>
                    </div>
                </div>
            </div>

            <div class="cell medium-4 large-4 center_img">
                <h1 class="tileindex">On vous attend !</h1>
            </div>
        <?php } ?>

        <?php
        foreach ($manager->getList(1, 1) as $news) {
            if (strlen($news->contenu()) <= 200) {
                $contenu = $news->contenu();
                $i = 0;
            } else {
                $debut = substr($news->contenu(), 0, 200);
                $debut = substr($debut, 0, strrpos($debut, ' ')) . '...';

                $contenu = $debut;
                $i = 0;
            }

        ?>

            <div class="cell medium-4 large-4">
                <div class="card contour">
                    <div class="card-section">
                        <img src="<?= $news->imagenews(); ?>" class="img_card">
                    </div>
                    <div class="card-section">
                        <p class="align-text"><?= nl2br($contenu); ?></p>
                    </div>
                </div>
            </div>


        <?php } ?>

    </div>

</div>
<div class="grid-container">
    <div class="grid-x grid-margin-x grid-padding-x">
        <?php
        foreach ($manager->getList(2, 4) as $news) {
            if (strlen($news->contenu()) <= 200) {
                $contenu = $news->contenu();
            } else {
                $debut = substr($news->contenu(), 0, 200);
                $debut = substr($debut, 0, strrpos($debut, ' ')) . '...';

                $contenu = $debut;
            }

        ?>

            <div class="cell medium-4 large-4">

                <div class="card contour">
                    <div class="card-section">
                        <img src="<?= $news->imagenews(); ?>" class="img_card">
                    </div>
                    <div class="card-section">
                        <p class="align-text"><?= nl2br($contenu); ?></p>
                    </div>
                </div>
            </div>
            <div class="cell medium-4 large-4 center_img">
            </div>
        <?php } ?>
    </div>

</div>
<?php require_once 'templates-parts/footer.php'; ?>