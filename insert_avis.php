<?php
//Permet de se connecter à la base de donnée
require 'connexionbdd.php'; 
//Permet d'activer le code au moment ou l'on cliquer sur le bouton 'Je m'inscris'
if(isset($_POST['formavis']))
{
    if(!empty($_POST['nompersonne']) AND !empty($_POST['commentaire']))
    {
        //Ici j'evite les ajouts de code SQL pour casser la bdd puis j'ajoute l'avis dans la bdd
        $nompersonne = htmlspecialchars($_POST['nompersonne']);
        $commentairepersonn = htmlspecialchars($_POST['commentaire']);
        $insert_avis = $bdd->prepare("INSERT INTO avis(nom_personne, commentaire) VALUES(?, ?)");
        $insert_avis->execute(array($nompersonne, $commentairepersonn));
        $erreur = "Votre avis a été envoyé !";
    }
    else
    {
        $erreur = "Veuillez remplir tous les champs !";
    }
}
?>