<!DOCTYPE html>
<html lang="fr">

<head>
    <?php require 'connexionbdd.php' ?>
    <?php session_start(); ?>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
    <?php require 'inscription_user_crepe.php'; ?>
</head>

<body>
    <main id="site-inscription">
        <?php require 'menu.php'; ?>
        <div class="grid-container">
            <div class="grid-x grid-margin-x grid-padding-x">
                <div class="cell small-6 medium-8 large-12">

                    <?php if (!empty($_SESSION['pseudo'])) {
                        //Affiche ce message d'erreur si la personne tente a tout prit d'acceder a Inscription
                        echo "<h2 class='dejaconnect'>Vous ne pouvez pas vous inscrire car vous êtes connecté !<button><a href='deconnexion.php'>Se déconnecter</a></button>";
                    } else { ?>
                </div>
                <div class="cell small-6 medium-8 large-12">
                    <h2 class="aligntext">Inscription</h2>
                    <form method="POST" action="">
                        <!--Un tableau est utilisé pour la fluidité et la mise en page du formulaire d'inscription -->
                        <table>
                            <tr>
                                <td>
                                    <!-- Le label permet d'afficher l'intitulé juste à coté du champ à remplir -->
                                    <label>Pseudo : </label>
                                </td>
                                <td>
                                    <input type="text" placeholder="Votre pseudo" name="pseudo" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Mail : </label>
                                </td>
                                <td>
                                    <input type="email" placeholder="Votre mail" name="mail" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Confirmer mail : </label>
                                </td>
                                <td>
                                    <input type="email" placeholder="Confirmer votre mail" name="mail2" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Mot de passe : </label>
                                </td>
                                <td>
                                    <input type="password" placeholder="Votre mot de passe" name="mdp" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Confirmer votre mot de passe : </label>
                                </td>
                                <td>
                                    <input type="password" placeholder="Confirmer votre mot de passe" name="mdp2" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input class="success button" name="forminscription" type="submit" value="Je m'inscris">
                                </td>
                            </tr>
                        </table>
                    </form>
                    <!--Le code PHP dessous permet d'afficher le message d'erreur si tous les champs ne sont pas remplie -->
                    <div class="erreur">
                        <?php
                        if (isset($erreur)) {
                            echo $erreur;
                        }
                        ?>
                    </div>

                <?php } ?>
                </div>
            </div>
        </div>



        </section>
    </main>
</body>

</html>