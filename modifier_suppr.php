<?php
session_start();
if (!empty($_SESSION['pseudo'])) {
    if ($_SESSION['id_role'] == 1) {
?>
        <!DOCTYPE html>
        <html lang="fr">

        <head>
            <link rel="stylesheet" href="assets/css/style.css">
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
        </head>

        <body>
            <main id="modif_suppr">
                <?php require 'menu.php'; ?>
                <div class="grid-container">
                    <div class="grid-x grid-margin-x grid-padding-x">
                        <div class="cell small-6 medium-8 large-12">
                            <h2 class="aligntext">Voici toutes les crêpes :</h2>
                            <table>
                                <tr>
                                    <td>ID :</td>
                                    <td>Nom :</td>
                                    <td>Ingredient :</td>
                                    <td>Prix(€) :</td>
                                    <td>Supprimer :</td>
                                    <td>Modifier :</td>
                                </tr>
                                <?php
                                require 'connexionbdd.php';
                                $reponse = $bdd->query('SELECT * FROM crepes');
                                $data = $reponse->fetchAll();
                                for ($i = 0; $i < count($data); $i++) {
                                    $id = $data[$i]['id'];
                                    $nom = $data[$i]['nom_crepe'];
                                    $ingredient = $data[$i]['ingredient'];
                                    $prix = $data[$i]['prix'];
                                    echo "<tr><td>$id</td><td>$nom</td><td>$ingredient</td><td>$prix</td>";
                                    echo "<td>";
                                    echo "<a href='crepe_suppr.php?id=$id'>Supprimer</a>";
                                    echo "</td>";
                                    echo "<td>";
                                    echo "<a href='crepe_modif.php?id=$id'>Modifier</a>";
                                    echo "</td>";
                                    echo "</tr>";
                                }
                                ?>
                            </table>
                    <?php
                }
            } else {
                echo "<script>alert(\"Vous n'êtes pas administrateur\")</script>";
                header("Location:index.php");
            }
                    ?>
                        </div>
                    </div>
                </div>
            </main>
        </body>

        </html>