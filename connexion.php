<!DOCTYPE html>
<html lang="fr">

<head>
    <?php require 'connexionbdd.php' ?>
    <?php session_start(); ?>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
    <?php require 'connexion_user_crepe.php'; ?>
</head>

<body>
    <main id="site-connexion">
        <?php require 'menu.php'; ?>
        <div class="grid-container">
            <div class="grid-x grid-margin-x grid-padding-x">
                <div class="cell small-6 medium-8 large-12">
                    <?php if (!empty($_SESSION['pseudo'])) {
                        //Affiche ce message d'erreur si la personne veut a tout pris rentrer sur la feuille connexion
                        echo "<h2 class='dejaconnect'>Vous ne pouvez pas vous connecter car vous êtes déjà connecté !<button><a href='deconnexion.php'>Se déconnecter</a></button>";
                    } else { ?>
                </div>
                <div class="cell small-6 medium-8 large-12">
                    <h2 class="aligntext">Connexion</h2>
                </div>
                <!-- Ici nous créons un formulaire pour permettre de l'utilisateur de se connecter -->
                <div class="cell medium-4 large-4">
                </div>
                <div class="cell medium-4 large-4">
                    <form method="POST" action="">
                        <input type="text" name="pseudoconnect" placeholder="Pseudo" />
                        <input type="password" name="mdpconnect" placeholder="Mot de passe" />
                        <input class="success button" type="submit" name="formconnect" value="Se connecter" />
                </div>
                <div class="cell medium-4 large-4">
                </div>
                </form>
                <div class="cell small-6 medium-8 large-12">
                    <div class="erreur">
                        <?php
                        if (isset($erreur)) {
                            echo $erreur;
                        } ?>
                    </div>
                </div>
            <?php } ?>
            </div>
        </div>
        </div>
    </main>
</body>

</html>