<!DOCTYPE html>
<html lang="fr">

<head>
    <?php require 'connexionbdd.php' ?>
    <?php session_start(); ?>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
    <?php require 'connexion_user_crepe.php'; ?>
</head>

<body>
    <main id="site-noscrepes">
        <!-- Permet d'afficher le menu -->
        <?php require 'menu.php'; ?>
        <div class="grid-container">
            <div class="grid-x grid-margin-x grid-padding-x">
                <div class="cell small-6 medium-8 large-12">
                    <h2 class="aligntext">Voici ce que nous pouvons vous proposez au restaurant :</h2>
                </div>
            </div>
            <div class="grid-x grid-margin-x grid-padding-x">
                <div class="cell small-6 medium-8 large-12">
                    <h2 class="aligntext">Les crêpes sucrées :</h2></a>
                </div>
                <?php
                //Ici je fais la requete pour allez chercher les crepes qui sont sucré car l'id est de 1
                $reponse = $bdd->query('SELECT * FROM crepes WHERE id_sucresale=1 ');
                while ($recup_crepesucre = $reponse->fetch()) {
                ?>
                    <div class="cell medium-4 large-4">
                        <div class="card" style="width: 300px;">
                            <div class="card-divider">
                                <!-- Ici j'affiche les crepes sucre  -->
                                <?= $recup_crepesucre['nom_crepe']; ?>
                            </div>
                            <img class="redim_photo" src="<?= $recup_crepesucre['chemin']; ?>">
                            <div class="card-section">
                                <h4><?= $recup_crepesucre['prix']; ?> €</h4>
                                <p>Les ingrédients : <?= $recup_crepesucre['ingredient']; ?></p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="grid-x grid-margin-x grid-padding-x">
                <div class="cell small-6 medium-8 large-12">
                    <h2 class="aligntext">Les crêpes salées :</h2></a>
                </div>
                <?php
                //Ici je fais la requete pour allez chercher les crepes qui sont sucré car l'id est de 2
                $reponse = $bdd->query('SELECT * FROM crepes WHERE id_sucresale=2 ');
                while ($recup_crepesale = $reponse->fetch()) {
                ?>
                    <div class="cell medium-4 large-4">
                        <div class="card" style="width: 300px;">
                            <div class="card-divider">
                                <?= $recup_crepesale['nom_crepe']; ?>
                            </div>
                            <img class="redim_photo" src="<?= $recup_crepesale['chemin']; ?>">
                            <div class="card-section">
                                <h4><?= $recup_crepesale['prix']; ?> €</h4>
                                <p>Les ingrédients : <?= $recup_crepesale['ingredient']; ?></p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </main>
</body>

</html>