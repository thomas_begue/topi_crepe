<?php
require 'connexionbdd.php';
session_start();
if (!empty($_SESSION['pseudo'])) {
    if ($_SESSION['id_role'] == 1) {


        //Permet d'activer le code au moment ou l'on cliquer sur le bouton 'J'ajoute une crêpe'
        if (isset($_POST['formajoutcrepe'])) {
            //Ici va vérifié si chaque input(Champ) est remplie
            if (!empty($_POST['nomcrep']) and !empty($_POST['ingredient']) and !empty($_POST['prix'])) {
                $nomcrep = $_POST['nomcrep'];
                $ingredient = $_POST['ingredient'];
                $prix = $_POST['prix'];
                $sucresale = $_POST['sucresale'];
                //Pour l'ajout d'image il faut impérativement que les images que vous UPLOADER SOIT DANS LE DOSSIER CREPES 
                $img = "assets/images/crepe/" . $_FILES['image']['name'];
                //Vérifie si le nom de la crêpe existe, si il n'existe pas alors on continue
                $existnom = $bdd->prepare("SELECT * FROM crepes WHERE nom = ?");
                $existnom->execute(array($nomcrep));
                $crepeexist = $existnom->RowCount();
                if ($crepeexist == 0) {
                    //Ici j'ajoute a ma base de donnée les valeurs que j'ai rentré dans le formulaire
                    $insert_crepe = $bdd->prepare("INSERT INTO crepes(nom_crepe, ingredient, prix, id_sucresale, chemin) VALUES(?, ?, ?, ?, ?)");
                    $insert_crepe->execute(array($nomcrep, $ingredient, $prix, $sucresale, $img));
                    $erreur = "La crêpe à bien été ajoutée";
                } else {
                    $erreur = "Une crepe porte déjà ce nom";
                }
            } else {
                $erreur = "Tous les champs ne sont pas remplis";
            }
        }
?>



        <!DOCTYPE html>
        <html lang="fr">

        <head>
            <link rel="stylesheet" href="assets/css/style.css">
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
        </head>

        <body>
            <main id="ajout-crepe">
                <?php require 'menu.php'; ?>
                <div class="grid-container">
                    <div class="grid-x grid-margin-x grid-padding-x">
                        <div class="cell small-6 medium-8 large-12">
                            <h2 class="aligntext">Ajouter une crêpe</h2>
                            <form method="POST" action="" enctype="multipart/form-data">
                                <!--Un tableau est utilisé pour la fluidité et la mise en page du formulaire d'inscription -->
                                <table>
                                    <tr>
                                        <td>
                                            <!-- Le label permet d'afficher l'intitulé juste à coté du champ à remplir -->
                                            <label>Nom : </label>
                                        </td>
                                        <td>
                                            <input type="text" placeholder="Nom crêpe" name="nomcrep" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Ingrédients : </label>
                                        </td>
                                        <td>
                                            <TEXTAREA type="text" name="ingredient" placeholder="Veuillez mettre les ingrédients" class="ingredtext"></TEXTAREA>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Prix : </label>
                                        </td>
                                        <td>
                                            <input type="text" placeholder="Prix €" name="prix" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Veuillez choisir entre sucré ou salé :</label>
                                        </td>
                                        <td>
                                            <select name="sucresale">
                                                <option value="1">Sucré</option>
                                                <option value="2">Salé</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Veuillez mettre l'image dans le répertoire crêpe avant de l'ajouter !</label>
                                        </td>
                                        <td>
                                            <input type="file" name="image" placeholder="Image" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <input class="success button" name="formajoutcrepe" type="submit" value="J'ajoute une crêpe">
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <!--Le code PHP dessous permet d'afficher le message d'erreur si tous les champs ne sont pas remplie -->
                            <div class="erreur">
                                <?php
                                if (isset($erreur)) {
                                    echo $erreur;
                                }
                                ?>
                            </div>
            </main>
    <?php
    } else {
        header("Location:index.php");
    }
} else {
    echo "<script>alert(\"Vous n'êtes pas administrateur\")</script>";
}
    ?>
        </body>

        </html>