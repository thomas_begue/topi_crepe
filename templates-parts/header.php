<!DOCTYPE html>
<html lang="fr">
<head>
<?php require 'connexionbdd.php' ?>
<?php session_start(); ?>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Affiche l'icone du site  -->
    <link rel="shortcut icon" type="image/ico" href="assets/images/logo_site.jpg"/>
    <!-- Va chercher la bibliothèque Fundation -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
    <!-- Appele mon CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
<header id="site-header">
<section id="allpage">
<?php require 'menu.php'; ?>

</header>
<main id="site-body">