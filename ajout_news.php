<?php
session_start();
if (!empty($_SESSION['pseudo'])) {
    if ($_SESSION['id_role'] == 1) {
        require 'menu.php';
        require 'lib/autoload.php';

        $db = DBFactory::getMysqlConnexionWithPDO();
        $manager = new NewsManagerPDO($db);

        if (isset($_GET['modifier'])) {
            $news = $manager->getUnique((int) $_GET['modifier']);
        }

        if (isset($_GET['supprimer'])) {
            $manager->delete((int) $_GET['supprimer']);
            $message = 'La news a bien été supprimée !';
        }

        if (isset($_POST['auteur'])) {
            $news = new News(
                [
                    'auteur' => $_POST['auteur'],
                    'titre' => $_POST['titre'],
                    'contenu' => $_POST['contenu'],
                    'imagenews' => "assets/images/news/" . $_FILES['imagenews']['name'],
                ]
            );

            if (isset($_POST['id'])) {
                $news->setId($_POST['id']);
            }

            if ($news->isValid()) {
                $manager->save($news);

                $message = $news->isNew() ? 'La news a bien été ajoutée !' : 'La news a bien été modifiée !';
            } else {
                $erreurs = $news->erreurs();
            }
        }

?>
        <!DOCTYPE html>
        <html lang="fr">

        <head>
            <link rel="stylesheet" href="assets/css/style.css">
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
        </head>

        <body>
            <main id="ajout_news">
                <div class="grid-container">
                    <div class="grid-x grid-padding-x grid-margin-x">
                        <div class="cell small-6 medium-8 large-12">
                            <h1 class="aligntext">Ajouter News</h1>
                        </div>
                    </div>
                </div>

                <p class="aligntext"><a href=".">Accéder à l'accueil du site</a> </p>
                <div class="grid-container">
                    <div class="grid-x grid-padding-x grid-margin-x">
                        <div class="cell small-6 medium-8 large-12">
                            <form action="ajout_news.php" method="post" enctype="multipart/form-data">


                                <?php
                                if (isset($message)) {
                                    echo $message, '<br />';
                                }
                                ?>
                                <?php if (isset($erreurs) && in_array(News::AUTEUR_INVALIDE, $erreurs)) echo 'L\'auteur est invalide.<br />'; ?>
                                Auteur : <input type="text" name="auteur" value="<?php if (isset($news)) echo $news->auteur(); ?>" /><br />

                                <?php if (isset($erreurs) && in_array(News::TITRE_INVALIDE, $erreurs)) echo 'Le titre est invalide.<br />'; ?>
                                Titre : <input type="text" name="titre" value="<?php if (isset($news)) echo $news->titre(); ?>" /><br />

                                <?php if (isset($erreurs) && in_array(News::IMAGENEWS_INVALIDE, $erreurs)) echo 'Le chemin de l\' est invalide.<br />'; ?>
                                Image : <input type="file" name="imagenews" value="<?php if (isset($news)) echo $news->imagenews(); ?>" /><br />

                                <?php if (isset($erreurs) && in_array(News::CONTENU_INVALIDE, $erreurs)) echo 'Le contenu est invalide.<br />'; ?>
                                Contenu :<br /><textarea rows="8" cols="60" name="contenu" class="ingredtext"><?php if (isset($news)) echo $news->contenu(); ?></textarea><br />
                                <?php
                                if (isset($news) && !$news->isNew()) {
                                ?>
                                    <input type="hidden" name="id" value="<?= $news->id() ?>" />
                                    <input type="submit" value="Modifier" name="modifier" />
                                <?php
                                } else {
                                ?>
                                    <input type="submit" value="Ajouter" />
                                <?php
                                }
                                ?>
                            </form>
                        </div>
                    </div>
                </div>
                <p style="text-align: center">Il y a actuellement <?= $manager->count() ?> news. En voici la liste :</p>

                <div class="grid-container">
                    <div class="grid-x grid-padding-x grid-margin-x">
                        <div class="cell small-6 medium-8 large-12">
                            <table>
                                <tr>
                                    <td>Auteur</td>
                                    <td>Titre</td>
                                    <td>Date d'ajout</td>
                                    <td>Dernière modification</td>
                                    <td>Action</td>
                                </tr>
                                <?php
                                foreach ($manager->getList() as $news) {
                                    echo '<tr><td>', $news->auteur(), '</td><td>', $news->titre(), '</td><td>', $news->dateAjout()->format('d/m/Y à H\hi'), '</td><td>', ($news->dateAjout() == $news->dateModif() ? '-' : $news->dateModif()->format('d/m/Y à H\hi')), '</td><td><a href="?modifier=', $news->id(), '">Modifier</a> | <a href="?supprimer=', $news->id(), '">Supprimer</a></td></tr>', "\n";
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
            </main>
        </body>
<?php
    } else {
        header("Location:index.php");
    }
} else {
    echo "<script>alert(\"Vous n'êtes pas administrateur\")</script>";
}
?>

        </html>